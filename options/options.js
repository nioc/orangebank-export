function saveOptions(e) {
  e.preventDefault();
  browser.storage.sync.set({
    bankId: document.querySelector("#bankId").value,
    branchId: document.querySelector("#branchId").value
  });
}

function restoreOptions() {

  function setCurrentChoice(result) {
    document.querySelector("#bankId").value = result.bankId || "18370";
    document.querySelector("#branchId").value = result.branchId || "00001";
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  var getting = browser.storage.sync.get(["bankId", "branchId"]);
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
